# Main Changelog of the project

## Repository Version: 0.0.1dev0

### Packages State

1. PHP - v.0.0.1dev0 on Packagist
2. Python - v.0.0.1dev0 on PyPi

### State

1. PHP - v.0.0.1dev0 
2. ENVFILES - v.0.0.1dev0
3. JSON Schema - v.0.0.1dev0
4. INI - v.0.0.1dev0
5. Docker - v.0.0.1dev0
6. TypeScript - v.0.0.1dev0
7. JavaScript - v.0.0.1dev0
8. <abbr title="XML Schema Definition">XSD</abbr>/DTD - v.0.0.1dev0
9. <abbr title="XML Stylesheet">XSL</abbr> - v.0.0.1dev0
10. XML - v.0.0.1dev0
11. .gitignore - v.0.0.1dev0
12. .gitlab-ci.yml - v.0.0.1dev0
13. JSON - v.0.0.1dev0
14. Stylus - v.0.0.1dev0
15. Sass - v.0.0.1dev0
16. <abbr title="Sassy CSS">SCSS</abbr> - v.0.0.1dev0
17. CSS - v.0.0.1dev0
18. Python - v.0.0.1dev0

### Changes

#### v.0.0.1dev0 [10.07.2018-20.07.2018]
Starting of Repository and some other things.