# PAiP Web Modules Project

## What is PWM Project?

PWM Project is project of library for languages listed below.
This library is just useful universal snippets.
For example curl in PHP is aweful for you then check repo is there module for it? No, then maybe write small class and contribute in making big snippets library?

Make your code <abbr title="Don't Repeat Yourself">DRY</abbr>.

## In other words

PWM is project of library with components for make small things like curl in PHP, MySQL in Python, FTP in Python or anything easier.

##### How it can affect your workflow?

Let's say you are working on project. But you are struggling for example with ..FTP in Python CLI Application. Then you can check that repo on Python Branch. Hmm.. let's see. Oh, Here it is FTP PWM.

## Why would you want to make code DRY?

1. Why you would want to repeat yourself?
    - It's not worth time and effort to continue on Repeating Yourself over and over again.
    - It's not worth money too.
2. Why you would want to make your project wet and heavy?
    - Heavier project isn't good..
        - If your project is heavier and heavier it's not good.
            If it's website it can load forever in web browser. 
            Even by deploying it by FTP it can take forever to upload.
            If it's CLI app it can be very bad in performance and very heavy (You don't want to make apps doing simple things such as uploading to Server Website using 50GB of space(only app i mean)).
            If it's data science project, Heavy project is not good too. Because you search for that you want to find forever.

## What languages we support now?

1. PHP
    - [Composer Package is on Packagist](https://packagist.org/packages/paipweb/pwm)
2. Python
    - [Python Package is on PyPi](https://pypi.org/project/pwm-lib/)
3. CSS
4. ENV (.ENV Templates)
5. JSON Schema
6. INI (.ini Templates)
7. Docker
8. TypeScript (GitLab Pipeline needed)
    - Package is planned to release on NPM
9. JavaScript (GitLab Pipeline needed)
    - Package is planned to release on NPM
10. XSD (XML Schema)
11. DTD (XML Schema - Older Version)
12. XSL & XSLT (XML Stylesheets & XML Transformation)
13. XML (XML Templates)
14. gitignore (.gitignore Templates)
15. GitLab CI (.gitlab-ci.yml Templates)
16. JSON (JSON Templates and useful snippets)
17. Stylus
18. SASS
19. SCSS

## What languages we will support?

1. Markdown (Templates or more)

## What languages we will think about?

1. C# (With GitLab Pipeline to compile it)
2. VisualBasic (With GitLab Pipeline to compile it)
3. C++

## License

This project is Open Source and under MIT License.